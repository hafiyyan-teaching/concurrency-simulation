package sharedmemory;

class Account{
    private int balance;

    public Account(int defaultBalance){
        this.balance = defaultBalance;
    }

    public void deposit(){
        this.balance = this.balance + 1;
    }

    public void withdraw(){
        this.balance = this.balance - 1;
    }

    public String getBalance(){
        return "Balance is "+Integer.toString(balance);
    }
}

class Bank{

    private Account account1;
    private Account account2;
    private Account account3;

    public Bank(){
        this.account1 = new Account(50);

        this.account2 = new Account(200);

        this.account3 = new Account(50);
    }

    public Account getAccount1(){
        return account1;
    }

    public Account getAccount2(){
        return account2;
    }

    public Account getAccount3(){
        return account3;
    }
}

class CashMachineExamplesSingleThread implements Runnable{

    private Bank bank;

    public CashMachineExamplesSingleThread(){
        this.bank = new Bank();
    }

    private void runSimulateBankTransaction(){
        Account account1 = bank.getAccount1();
        Account account4 = new Account(150);
        for (int i = 0; i < 5; i++){
            account1.withdraw();
            bank.getAccount2().deposit();
            account4.withdraw();

            System.out.println("Account 1 retrieved from Bank and locally stored in "+Thread.currentThread().getName()+", have "+account1.getBalance());
            System.out.println("Account 2 retrieved from Bank in "+Thread.currentThread().getName()+", have "+bank.getAccount2().getBalance());
            System.out.println("Account 4 locally created in "+Thread.currentThread().getName()+", have "+account4.getBalance());

        }
    }

    @Override
    public void run() {
        this.runSimulateBankTransaction();
    }
}


public class AccountSimulator {
    public static void main(String[] args){
        CashMachineExamplesSingleThread cashMachineExamplesSingleThread = new CashMachineExamplesSingleThread();

        Thread t1 = new Thread(cashMachineExamplesSingleThread, "Thread-1");
        Thread t2 = new Thread(cashMachineExamplesSingleThread, "Thread-2");
        t1.start();
        t2.start();

    }
}
